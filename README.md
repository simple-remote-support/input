# input



## Getting started

clone the repo with   
```
git clone https://gitlab.com/simple-remote-support/input.git
cd input
git submodule init
git submodule update
```
build the wsServer library  
```
cd wsServer
mkdir bin
cd bin
cmake ..
make 
```
copy the wsServer lib to to libs folder  
```
cd ..
cd ..
mkdir libs
cp ./wsServer/bin/libws.a ./libs/
```

compile the srs input binary  
```
mkdir bin
gcc -o bin/srsinput srsinput.c -Llibs -lws `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0` 

```
