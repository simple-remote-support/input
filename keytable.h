#ifndef KEYTABLE_H
#define KEYTABLE_H

#include <stdio.h>
#include <stdlib.h>
#include <search.h>

static struct keypair {
	char *key;
	unsigned short value;
} keytable[] = {
	{ "Backquote", 1 },
	{ "Digit1", 2 },
	{ "Digit2", 3 },
	{ "Digit3", 4 },
	{ "Digit4", 5 },
	{ "Digit5", 6 },
	{ "Digit6", 7 },
	{ "Digit7", 8 },
	{ "Digit8", 9 },
	{ "Digit9", 10 },
	{ "Digit0", 11 },
	{ "Minus", 12 },
	{ "Equal", 13 },
	{ "Backspace", 14 },
	{ "Tab", 15 },
	{ "KeyQ", 16 },
	{ "KeyW", 17 },
	{ "KeyE", 18 },
	{ "KeyR", 19 },
	{ "KeyT", 20 },
	{ "KeyY", 21 },
	{ "KeyU", 22 },
	{ "KeyI", 23 },
	{ "KeyO", 24 },
	{ "KeyP", 25 },
	{ "BracketLeft", 26 },
	{ "BracketRight", 27 },
	{ "Enter", 28 },
	{ "ControlLeft", 29 },
	{ "KeyA", 30 },
	{ "KeyS", 31 },
	{ "KeyD", 32 },
	{ "KeyF", 33 },
	{ "KeyG", 34 },
	{ "KeyH", 35 },
	{ "KeyJ", 36 },
	{ "KeyK", 37 },
	{ "KeyL", 38 },
	{ "Semicolon", 39 },
	{ "Quote", 40 },
	{ "Backslash", 41 },
	{ "ShiftLeft", 42 },
	{ "IntlBackslash", 43 },
	{ "KeyZ", 44 },
	{ "KeyX", 45 },
	{ "KeyC", 46 },
	{ "KeyV", 47 },
	{ "KeyB", 48 },
	{ "KeyN", 49 },
	{ "KeyM", 50 },
	
	{ "Comma", 51 },
	{ "Period", 52 },
	{ "Slash", 53 },
	{ "ShiftRight", 54 },
	{ "NumpadMultiply", 55 },
	{ "AltLeft", 56 },
	{ "Space", 57 },
	{ "CapsLock", 58 },
	{ "F1", 59 },
	{ "F2", 60 },
	{ "F3", 61 },
	{ "F4", 62 },
	{ "F5", 63 },
	{ "F6", 64 },
	{ "F7", 65 },
	{ "F8", 66 },
	{ "F9", 67 },
	{ "F10", 68 },	
	{ "NumLock", 69 },
	{ "ScrollLock", 70 },
	{ "Numpad7", 71 },
	{ "Numpad8", 72 },
	{ "Numpad9", 73 },
	{ "NumpadMinus", 74 },
	{ "Numpad4", 75 },
	{ "Numpad5", 76 },
	{ "Numpad6", 77 },
	{ "NumpadPlus", 78 },
	{ "Numpad1", 79 },
	{ "Numpad2", 80 },
	{ "Numpad3", 81 },
	{ "Numpad0", 82 },
	{ "NumpadDecimal", 83 },
	{ "F11", 87 },
	{ "F12", 88 },
	{ "NumpadEnter", 96 },
	{ "ControlRight", 97 },
	{ "NumpadDivide", 98 },
	{ "AltRight", 100 },
	
	{ "Home", 102 },
	{ "ArrowUp", 103 },
	{ "PageUp", 104 },
	{ "ArrowLeft", 105 },
	{ "ArrowRight", 106 },
	{ "End", 107 },
	{ "ArrowDown", 108 },
	{ "PageDown", 109 },
	{ "Insert", 110 },
	{ "Delete", 111 }
};

void initKeyTable(){
	ENTRY e;
	ENTRY *ep;
	hcreate(255);
	int tableSize = sizeof(keytable) / sizeof(struct keypair);
	for (int i = 0; i < tableSize; i++){
		e.key = keytable[i].key;
		e.data = &keytable[i];
		ep = hsearch(e, ENTER);
		if (ep == NULL) {
			fprintf(stderr, "entry failed\n");
			exit(EXIT_FAILURE);
		}
	}
}

void testKeyTable() {
	ENTRY e;
	ENTRY *ep;
	int tableSize = sizeof(keytable) / sizeof(struct keypair);
	for (int i = 0; i < tableSize; i++) {
		e.key = keytable[i].key;
		ep = hsearch(e, FIND);
		printf("%9.9s -> %9.9s : %d\n", e.key, ep ? ep->key : "NULL", ep ? ((struct keypair*)ep->data)->value : 0);
	}
}

unsigned short getKey(char *key){
	ENTRY e;
	ENTRY *ep;
	e.key = key;
	ep = hsearch(e, FIND);
	struct keypair *kp = (struct keypair*)ep->data;
	return ep ? kp->value : 0;
}

#endif
