#include <linux/uinput.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include "wsServer/include/ws.h"
#include "keytable.h"


#define MOUSEMOVEMESSAGE 1
#define MOUSEBUTTONMESSAGE 2
#define KEYPRESSEDMESSAGE 3
#define REQUESTCONTROL 4
#define CONTROLGRANTED 5
#define CONTROLDENIED 6

int fd;
bool controlGranted = false;

gint TimerCallback(){
	gtk_main_quit();
	return 0;
}

void showdialog(ws_cli_conn_t *client){
    GtkWidget* dialog = gtk_message_dialog_new(
        NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_OK_CANCEL,
        "Allow remote access to mouse and keyboard");

    gtk_message_dialog_format_secondary_text(
        GTK_MESSAGE_DIALOG(dialog),
        "By pressing OK you will grant access to your desktop");

    int response = gtk_dialog_run(GTK_DIALOG(dialog));

		if (response == GTK_RESPONSE_OK) {
			controlGranted = true;
			ws_sendframe_txt(client, "{\"d\":5}");
			printf("control Granted\n");
		} else {
			controlGranted = false;
			ws_sendframe_txt(client, "{\"d\":6}");
			printf("control denied\n");
		}
    gtk_widget_destroy(dialog);
    // -- starting the gtk_main event loop and adding a timer to call gtk_main_quit -- 
		g_timeout_add (100, TimerCallback, NULL);
    gtk_main();
	  // -- this is needed to clear the dialog window  --
}

/**
 * @brief Emits input device events
 * @param fd     Device file descriptor.
 * @param type   Event type.
 * @param code   Event code.
 * @param value  Event value.
 */
void emit(int fd, int type, int code, int val)
{
   struct input_event ie;
   ie.type = type;
   ie.code = code;
   ie.value = val;
   /* timestamp values below are ignored */
   ie.time.tv_sec = 0;
   ie.time.tv_usec = 0;
   write(fd, &ie, sizeof(ie));
}

/**
 * @brief Handle Mouse button message
 * @param msg    Message content.
 * @param size   Message size.
 */
void handleMouseButton(const unsigned char *msg, uint64_t size){
	if (size<=2 || !controlGranted) return;
	int btn = BTN_MOUSE + msg[1];
	int val = msg[2];
	emit(fd, EV_KEY, btn, val);
	emit(fd, EV_SYN, SYN_REPORT, 0);
}
/**
 * @brief Handle Mouse movement message
 * @param msg    Message content.
 * @param size   Message size.
 */
void handleMouseMove(const unsigned char *msg, uint64_t size){
	if (size<=2 || !controlGranted) return;
	int dx = (char)msg[1];
	int dy = (char)msg[2];
	emit(fd, EV_REL, REL_X, dx);
	emit(fd, EV_REL, REL_Y, dy);
	emit(fd, EV_SYN, SYN_REPORT, 0);
}
/**
 * @brief Handle keyboard messages
 * @param msg    Message content.
 * @param size   Message size.
 */
void handleKeyboard(const unsigned char *msg, uint64_t size){
	if (size<=2 || !controlGranted) return;
	int val = msg[1];
	char key[size-1];
	for (int i = 2; i < size; i++) key [i-2] = msg[i];
	key[size-2] = 0;
	int btn = getKey((char*)&key);
	if (!btn) return;
	emit(fd, EV_KEY, btn, val);
	emit(fd, EV_SYN, SYN_REPORT, 0);
}

/**
 * @brief Handle keyboard messages
 * @param client Client connection.
 * @param msg    Message content.
 * @param size   Message size.
 */
void handleControlRequest(ws_cli_conn_t *client, const unsigned char *msg, uint64_t size){
	
	showdialog(client);
}

/**
 * @brief This function is called whenever a new connection is opened.
 * @param client Client connection.
 */
void onOpen(ws_cli_conn_t *client)
{
	char *cli;
	cli = ws_getaddress(client);
	printf("Connection opened, addr: %s\n", cli);
}

/**
 * @brief This function is called whenever a connection is closed.
 * @param client Client connection.
 */
void onClose(ws_cli_conn_t *client) {
	char *cli;
	cli = ws_getaddress(client);
	printf("Connection closed, addr: %s\n", cli);
	unsigned char msg[] = {0,0,0}; 
	handleMouseButton((unsigned char *)&msg, 3);
	msg[1] = 1;
	handleMouseButton((unsigned char *)&msg, 3);
	controlGranted = false;
}

/**
 * @brief Message events goes here.
 * @param client Client connection.
 * @param msg    Message content.
 * @param size   Message size.
 * @param type   Message type.
 */
void onMessage(ws_cli_conn_t *client, const unsigned char *msg, uint64_t size, int type) {
	char *cli;
	cli = ws_getaddress(client);
	if (size == 0) return;
	switch (msg[0]) {
		case MOUSEMOVEMESSAGE:
			handleMouseMove(msg, size);
			break;
		case MOUSEBUTTONMESSAGE:
			handleMouseButton(msg, size);
			break;
		case KEYPRESSEDMESSAGE:
			handleKeyboard(msg, size);
			break;
		case REQUESTCONTROL:
			handleControlRequest(client, msg, size);
			break;
	}	
}



int main(int argc, char** argv)
{
	struct uinput_setup usetup;
	struct ws_events evs;	
	gtk_init(&argc, &argv);

	int i = 50;
	int port = 7784;
	initKeyTable();
	//testKeyTable();
	
	fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);

	/* --- uinput --- */
	/* --- mouse --- */
	ioctl(fd, UI_SET_EVBIT, EV_KEY);
	ioctl(fd, UI_SET_KEYBIT, BTN_LEFT);
	
	ioctl(fd, UI_SET_EVBIT, EV_KEY);
	ioctl(fd, UI_SET_KEYBIT, BTN_RIGHT);

	ioctl(fd, UI_SET_EVBIT, EV_REL);
	ioctl(fd, UI_SET_RELBIT, REL_X);
	ioctl(fd, UI_SET_RELBIT, REL_Y);
	
		/* --- keyboard --- */
	ioctl(fd, UI_SET_EVBIT, EV_REP);
  for (i=0; i < 256; i++) {
      ioctl(fd, UI_SET_KEYBIT, i);
  }

	memset(&usetup, 0, sizeof(usetup));
	usetup.id.bustype = BUS_USB;
	usetup.id.vendor = 0x1234; /* sample vendor */
	usetup.id.product = 0x5678; /* sample product */
	strcpy(usetup.name, "SRS input device");

	ioctl(fd, UI_DEV_SETUP, &usetup);
	ioctl(fd, UI_DEV_CREATE);
	
	/* --- webocket --- */
	evs.onopen    = &onOpen;
	evs.onclose   = &onClose;
	evs.onmessage = &onMessage;
	ws_socket(&evs, port, 0, 1000);
	
	ioctl(fd, UI_DEV_DESTROY);
	close(fd);

	return 0;
}
